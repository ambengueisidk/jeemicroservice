package com.mbengue.gatwaypatient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GatwayPatientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatwayPatientApplication.class, args);
    }

}

package com.mbengue.gatwaypatient;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController

public class FallbackController {
    @RequestMapping("/visiteFallback")
    public Mono<String> visiteServiceFAll(){
        return Mono.just("Le service Visite prend du temps à répondre. Veuillez réessayer ultérieurement.");
        }
    @RequestMapping("/patientFallback")
    public Mono<String> patientServiceFallback(){
        return Mono.just("Le service patient prend du temps à répondre. Veuillez réessayer ultérieurement.");
    }
}

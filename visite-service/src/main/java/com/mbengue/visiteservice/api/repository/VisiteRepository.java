package com.mbengue.visiteservice.api.repository;


import com.mbengue.visiteservice.api.entity.Visite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisiteRepository extends JpaRepository<Visite,Long> {
     Visite findByRv(String rv);
     Visite findBydateVisite(String dateVisite);
    //public Visite findBypatient();
    //public Visite findBymedecin();
    //public Visite findBynote();
}
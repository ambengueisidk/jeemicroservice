package com.mbengue.visiteservice.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class VisiteServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VisiteServiceApplication.class, args);
    }

}

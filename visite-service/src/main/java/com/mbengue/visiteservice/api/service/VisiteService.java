package com.mbengue.visiteservice.api.service;


import com.mbengue.visiteservice.api.entity.Visite;
import com.mbengue.visiteservice.api.repository.VisiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class VisiteService {
    @Autowired
    private VisiteRepository visiteRepository;
    public Visite saveVisite(Visite visite){
        return visiteRepository.save(visite);
    }
    public Visite findByRv(String rv){
        return visiteRepository.findByRv(rv);
    }
    public Visite findByDateVisite(String dateV){
        return visiteRepository.findBydateVisite(dateV);
    }

}

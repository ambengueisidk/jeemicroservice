package com.mbengue.visiteservice.api.controller;

import com.mbengue.visiteservice.api.entity.Visite;
import com.mbengue.visiteservice.api.service.VisiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/visite")
public class VisiteController {
    @Autowired
    private VisiteService visitService;
    @PostMapping("/addVisite")
    public Visite save(@RequestBody Visite visite){
        return visitService.saveVisite(visite);
    }/*
    @RequestMapping("/getOnVisite/{rv}")
    public Visite getByRv(@PathVariable String rv){
        return visitService.findByRv(rv);
    }*/
    @RequestMapping("/getOnVisite/{dateV}")
    public Visite getByDateVisite(@PathVariable String dateV){
        return visitService.findByDateVisite(dateV);
    }

}
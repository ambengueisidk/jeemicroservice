package com.mbengue.patient.api.controller;

import com.mbengue.patient.api.entity.Patient;
import com.mbengue.patient.api.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    private PatientService patientService;
    @PostMapping("/addPatient")
    public Patient save(@RequestBody Patient patient){
        return patientService.savePatient(patient);
    }
}

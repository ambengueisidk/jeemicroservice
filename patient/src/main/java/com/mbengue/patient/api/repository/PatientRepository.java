package com.mbengue.patient.api.repository;

import com.mbengue.patient.api.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient,Long> {
}

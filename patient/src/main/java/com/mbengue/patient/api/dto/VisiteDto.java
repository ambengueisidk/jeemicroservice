package com.mbengue.patient.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisiteDto {
    private Long id;
    private String dateVisite;
    private String rv;
    private String patient;
    private String medecin;
    private String note;
}

package com.mbengue.patient.api.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Patient {
    @Id
    @GeneratedValue
    private Long idPatient;
    private String email;
    private String prenom;
    private String nom;
    private String adresse;
    private String cni;
    private String dateNaiss;
    private String telephone;
    private String dateVisite;
}

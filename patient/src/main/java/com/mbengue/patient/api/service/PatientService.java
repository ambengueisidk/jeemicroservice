package com.mbengue.patient.api.service;

import com.mbengue.patient.api.dto.VisiteDto;
import com.mbengue.patient.api.entity.Patient;
import com.mbengue.patient.api.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private RestTemplate restTemplate;
    public Patient savePatient(Patient patient){
        String responseTest = "";
        //VisiteDto visiteDto= restTemplate.getForObject("http://localhost:8181/visite/getOnVisite/{id}", VisiteDto.class, patient.getRvVisite());
        //VisiteDto visiteDto= restTemplate.getForObject("http://VISITE-SERVICE/visite/getOnVisite/{id}", VisiteDto.class, patient.getRvVisite());
        //patient.setRvVisite(visiteDto.getRv());
        VisiteDto visiteDto= restTemplate.getForObject("http://VISITE-SERVICE/visite/getOnVisite/{id}", VisiteDto.class, patient.getDateVisite());
        patient.setDateVisite(visiteDto.getDateVisite());
        System.out.println("RESPOOOOOOOONSE : "+visiteDto);
        patientRepository.save(patient);
        return patient;
    }

}
